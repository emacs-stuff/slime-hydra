;;; Set of hydra menus to work with Slime.
;;; vindarel 2017
;;; copyleft

(defhydra slime-hydra (:color red)
    "
     Slime
"
    ("." slime-edit-definition "edit definition")
    ("," slime-pop-find-definition-stack "return from definition")
    ("s" slime-selector-hydra/body "selector" :color blue)
    ;; evaluation
    ;; debugging
    ;; compilation
    ;; cross reference
    ;; editing
    ;; …
    )

(defun slime-selector-call-by-key (key)
  "Call a slime-selector function associated with the given character."
  ;; Strangely, this code is obscured in slime.el. Functions are not
  ;; defined by name.
  (funcall (cl-third (cl-find key slime-selector-methods :key #'car))))

(defhydra slime-selector-hydra (:color red
                                :columns 4)
  " Slime selector "
  ("4" (slime-selector-call-by-key ?4) "other window")
  ("c" (slime-selector-call-by-key ?c) "connections buffer")
  ("d" (slime-selector-call-by-key ?d) "*sldb* buffer for the current connection")
  ("e" (slime-selector-call-by-key ?e) "most recently emacs-lisp-mode buffer")
  ("i" (slime-selector-call-by-key ?i) "*inferior-lisp* buffer")
  ("l" (slime-selector-call-by-key ?l) "most recently visited lisp-mode buffer")
  ("n" (slime-selector-call-by-key ?n) "next Lisp connection")
  ("p" (slime-selector-call-by-key ?p) "previous Lisp connection")
  ("r" (slime-selector-call-by-key ?r) "REPL")
  ("s" (slime-selector-call-by-key ?s) "*slime-scratch* buffer")
  ("t" (slime-selector-call-by-key ?t) "threads buffer")
  ("v" (slime-selector-call-by-key ?v) "*slime-events* buffer")
  ("q" nil "quit")
  ("S" slime-hydra/body "Slime hydra" :color blue)
  )
